$(document).ready(function(){
    var cameraRequirementContainer = $('#cameraRequirementChecker')
    var cameraRequirementForm = $('#cameraRequirementCheckerForm')
    var cameraRequirementResults = $('#cameraRequirementCheckerResults')
    var verboseLogsContainer = $('#verbose-logs')
    var canvasDiskUsageContainer = $('#canvas-disk-usage')
    var canvasGeneralUsageContainer = $('#canvas-general-usage')
    var simulatedCpuInformation = $('#simulatedCpuInformation')
    var simualtedCpuSelector = $('#simualtedCpuSelector')
    var simulateableCpus = [
        {
            ramRequiredPerProcess: 6,
            baseUtilizationPerProcess: 0.7,
            diskSpaceBitrateDivisor: 100,
            totalRam: 3851.44,
            operatingSystem: 'Ubuntu 19.10',
            note: 'No Encoding, Passthrough Stream and Recording.',
            lscpu: `Architecture:        x86_64
            CPU op-mode(s):      32-bit, 64-bit
            Byte Order:          Little Endian
            CPU(s):              4
            On-line CPU(s) list: 0-3
            Thread(s) per core:  2
            Core(s) per socket:  2
            Socket(s):           1
            NUMA node(s):        1
            Vendor ID:           GenuineIntel
            CPU family:          6
            Model:               58
            Model name:          Intel(R) Core(TM) i5-3210M CPU @ 2.50GHz
            Stepping:            9
            CPU MHz:             2887.290
            CPU max MHz:         3100.0000
            CPU min MHz:         1200.0000
            BogoMIPS:            4988.29
            Virtualization:      VT-x
            L1d cache:           32K
            L1i cache:           32K
            L2 cache:            256K
            L3 cache:            3072K
            NUMA node0 CPU(s):   0-3
            Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm cpuid_fault pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid
                                 fsgsbase smep erms xsaveopt dtherm ida arat pln pts md_clear flush_l1d`
        },
        {
            ramRequiredPerProcess: 6,
            baseUtilizationPerProcess: 0.7,
            diskSpaceBitrateDivisor: 100,
            totalRam: 16043.92,
            operatingSystem: 'Ubuntu 19.10',
            note: 'No Encoding, Passthrough Stream and Recording.',
            lscpu: `Architecture:                    x86_64
            CPU op-mode(s):                  32-bit, 64-bit
            Byte Order:                      Little Endian
            Address sizes:                   48 bits physical, 48 bits virtual
            CPU(s):                          8
            On-line CPU(s) list:             0-7
            Thread(s) per core:              2
            Core(s) per socket:              4
            Socket(s):                       1
            NUMA node(s):                    1
            Vendor ID:                       AuthenticAMD
            CPU family:                      21
            Model:                           2
            Model name:                      AMD FX-8370 Eight-Core Processor
            Stepping:                        0
            Frequency boost:                 enabled
            CPU MHz:                         1405.215
            CPU max MHz:                     4000.0000
            CPU min MHz:                     1400.0000
            BogoMIPS:                        8036.89
            Virtualization:                  AMD-V
            L1d cache:                       64 KiB
            L1i cache:                       256 KiB
            L2 cache:                        8 MiB
            L3 cache:                        8 MiB
            NUMA node0 CPU(s):               0-7
            Vulnerability Itlb multihit:     Not affected
            Vulnerability L1tf:              Not affected
            Vulnerability Mds:               Not affected
            Vulnerability Meltdown:          Not affected
            Vulnerability Spec store bypass: Mitigation; Speculative Store Bypass disabled via prctl and seccomp
            Vulnerability Spectre v1:        Mitigation; usercopy/swapgs barriers and __user pointer sanitization
            Vulnerability Spectre v2:        Mitigation; Full AMD retpoline, IBPB conditional, STIBP disabled, RSB filling
            Vulnerability Tsx async abort:   Not affected
            Flags:                           fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush mmx fxsr sse sse2 ht syscall nx mmxext fxsr_opt pdpe1gb rdtscp lm
                                              constant_tsc rep_good nopl nonstop_tsc cpuid extd_apicid aperfmperf pni pclmulqdq monitor ssse3 fma cx16 sse4_1 sse4_2 popcnt aes xsave avx f16c
                                              lahf_lm cmp_legacy svm extapic cr8_legacy abm sse4a misalignsse 3dnowprefetch osvw ibs xop skinit wdt fma4 tce nodeid_msr tbm topoext perfctr_co
                                             re perfctr_nb cpb hw_pstate ssbd ibpb vmmcall bmi1 arat npt lbrv svm_lock nrip_save tsc_scale vmcb_clean flushbyasid decodeassists pausefilter pf
                                             threshold`
        }
    ]
    var parseSimuateableCpuString = function(string){
        var data = {}
        var lastKey = ''
        string.split('\n').forEach(function(line){
            var lineParts = line.split(':')
            var key = lineParts[0].trim()
            var value = lineParts[1] ? lineParts[1].trim() : null
            if(!value){
                data[lastKey] += ' ' + key.trim()
                return
            }
            var parsedFloatValue = parseFloat(value)
            data[key] = isNaN(parsedFloatValue) ? value : parsedFloatValue
        })
        if(data['Flags'])data['Flags'] = data['Flags'].split(' ')
        return data
    }
    var roundValue = function(val){
        return parseFloat(val).toFixed(2)
    }
    var drawDiskUsedTable = function(calculation){
        var html = `<table class="table table-striped">`
        $.each(calculation.diskSpaceLabel,function(n,label){
            html += `<tr>
                <td style="width:30%">${label}</td>
                <td class="text-right">${calculation.diskSpace[n] + ` ${calculation.diskSpaceUnit[n]}`}</td>
            </tr>`
        })
        html += `</table>`
        canvasDiskUsageContainer.append(html)
    }
    var drawNetCpuRamUsedTable = function(calculation){
        var html = `<table class="table table-striped">`
        $.each({
            "Network Bandwidth": calculation.networkBandwidth + ` ${calculation.networkBandwidthUnit}`,
            "CPU Utilization": calculation.cpuUtilization + `${calculation.cpuUtilizationUnit}`,
            "RAM Utilization": calculation.ram + ` ${calculation.ramUnit}`,
        },function(label,value){
            html += `<tr>
                <td style="width:30%">${label}</td>
                <td class="text-right">${value}</td>
            </tr>`
        })
        html += `</table>`
        canvasGeneralUsageContainer.append(html)
    }
    var createChart = function(calculation){
		var randomScalingFactor = function() {
			return Math.round(Math.random() * 100);
		};

		var color = Chart.helpers.color;
		var config = {
			type: 'radar',
			data: {
				labels: [
                    `Network Bandwidth (${calculation.networkBandwidthUnit})`,
                    `CPU Utilization (${calculation.cpuUtilizationUnit})`,
                    `RAM (${calculation.ramUnit})`,
                ],
				datasets: [{
					label: 'My First dataset',
                    backgroundColor: [
                        color(chartColors.red).alpha(0.5).rgbString(),
                        color(chartColors.green).alpha(0.5).rgbString(),
                        color(chartColors.blue).alpha(0.5).rgbString(),
                    ],
					data: [
                        calculation.networkBandwidth,
                        calculation.cpuUtilization,
                        calculation.ram,
					]
				}]
			},
			options: {
				legend: {
					position: 'top',
				},
				title: {
					display: true,
					text: 'Estimated Network, CPU, and RAM Use'
				},
				scale: {
					ticks: {
						beginAtZero: true
					}
				}
			}
		};
        var ctx = canvasGeneralUsageContainer.find('canvas')[0];
		Chart.PolarArea(ctx, config);
//

		var color = Chart.helpers.color;
        var labels = []
        $.each(calculation.diskSpaceLabel,function(n,label){
            labels.push(label + ' (' + calculation.diskSpaceUnit[n] + ')')
        })
		var barChartData = {
			labels: labels,
			datasets: [{
				label: 'Disk Use',
				backgroundColor: color(window.chartColors.red).alpha(0.5).rgbString(),
				borderColor: window.chartColors.red,
				borderWidth: 1,
				data: Object.values(calculation.diskSpace)
			}]

		};
        var ctx = canvasDiskUsageContainer.find('canvas')[0];
        new Chart(ctx, {
            type: 'bar',
            data: barChartData,
            options: {
                responsive: true,
                legend: {
                    position: 'top',
                },
                title: {
                    display: true,
                    text: 'Estimated Disk Use'
                }
            }
        })
    }
    var calculateRequirements = function(options){
        // assuming CBR and no encoding
        // assuming x86_x64 CPU
        // assuming H264 streams being consumed
        // assuming Streaming out as HLS (consumes most RAM)
        // assumed day time, many colors
        // moderate motion outside, expected event every 1 hour at minimum and every 30 minutes at maximum
        // all calculations are approximations on a generalized set of hardware
        // Mac Mini : Intel(R) Core(TM) i5-3210M CPU @ 2.50GHz - 4 Cores



        var fields = {}
        cameraRequirementForm.find('input, select, textarea').each(function() {
            fields[this.name] = $(this).val();
        });
        var selectedCpu = simulateableCpus[parseInt(fields.simualtedCpu)]
        var newOptions = Object.assign({
            ramRequiredPerProcess: 4, //mb
            baseUtilizationPerProcess: 0.7, //%
            diskSpaceBitrateDivisor: 100, //100 = Constant Change, 130 = Moderate Motion
        }, selectedCpu, options || {})
        var cameraCount = parseInt(fields.cameraCount) || 1
        // var fps = parseInt(fields.fps) || 15
        var bitrate = parseInt(fields.bitrate) || 2000
        // var resolution = fields.resolution || '1920x1080'
        var utilizationPerCameraAdditive = newOptions.baseUtilizationPerProcess / bitrate  //%

        var networkBandwidthRequirement = cameraCount * bitrate
        var cpuUtilization = cameraCount * (newOptions.baseUtilizationPerProcess + utilizationPerCameraAdditive)
        var ramRequirement = newOptions.ramRequiredPerProcess * cameraCount
        var diskSpaceRequirement = cameraCount * bitrate / newOptions.diskSpaceBitrateDivisor //per minute of recording
        var calculation = {
            networkBandwidth: networkBandwidthRequirement >= 1000 ? networkBandwidthRequirement / 1000 : networkBandwidthRequirement,
            networkBandwidthUnit: networkBandwidthRequirement >= 1000 ? 'Mbits/s' : 'Kbits/s',
            cpuUtilization: roundValue(cpuUtilization),
            cpuUtilizationUnit: '%',
            simulatedRamTotal: newOptions.totalRam,
            ram: ramRequirement,
            ramUnit: 'MB',
            diskSpace: {
                perMinute: roundValue(diskSpaceRequirement),
                perHour: roundValue(diskSpaceRequirement * 60),
                perDay: roundValue(diskSpaceRequirement * 60 * 24),
                perWeek: roundValue(diskSpaceRequirement * 60 * 24 * 7),
                perYear: roundValue(diskSpaceRequirement * 60 * 24 * 365),
            },
            diskSpaceLabel: {
                perMinute: `Per Minute`,
                perHour: `Per Hour`,
                perDay: `Per Day`,
                perWeek: `Per Week`,
                perYear: `Per Year`,
            },
            diskSpaceUnit: {},
            optionsUsed: newOptions
        }
        $.each(calculation.diskSpace,function(key,amount){
            calculation.diskSpaceUnit[key] = amount / 1000 >= 1000 ? 'TB' : amount >= 1000 ? 'GB' : 'MB'
            calculation.diskSpace[key] = amount / 1000 >= 1000 ? amount / 1000000  : amount >= 1000 ? amount / 1000 : amount
        })
        return calculation
    }
    var resetCalculation = function(){
        $('#canvas-disk-usage, #canvas-general-usage').html('<canvas></canvas>')
    }
    var drawCalculation = function(){
        var requirements = calculateRequirements()
        resetCalculation();
        createChart(requirements)
        drawDiskUsedTable(requirements)
        drawNetCpuRamUsedTable(requirements)
        verboseLogsContainer.html('<code>' + JSON.stringify(requirements,null,3) + '</code>')
    }
    cameraRequirementForm.find('select').change(function(){
        drawCalculation()
    })
    cameraRequirementForm.find('input').keyup(function(){
        drawCalculation()
    })
    $.each(simulateableCpus,function(n,cpu){
        cpu.lscpu = parseSimuateableCpuString(cpu.lscpu)
        simualtedCpuSelector.append(`<option ${n === 0 ? 'selected' : ''} value="${n}">${cpu.name ? cpu.name : cpu.lscpu['Model name']}</option>`)
    })
    drawCalculation()
})


// lscpu
// Architecture:        x86_64
// CPU op-mode(s):      32-bit, 64-bit
// Byte Order:          Little Endian
// CPU(s):              4
// On-line CPU(s) list: 0-3
// Thread(s) per core:  2
// Core(s) per socket:  2
// Socket(s):           1
// NUMA node(s):        1
// Vendor ID:           GenuineIntel
// CPU family:          6
// Model:               58
// Model name:          Intel(R) Core(TM) i5-3210M CPU @ 2.50GHz
// Stepping:            9
// CPU MHz:             2887.290
// CPU max MHz:         3100.0000
// CPU min MHz:         1200.0000
// BogoMIPS:            4988.29
// Virtualization:      VT-x
// L1d cache:           32K
// L1i cache:           32K
// L2 cache:            256K
// L3 cache:            3072K
// NUMA node0 CPU(s):   0-3
// Flags:               fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx rdtscp lm constant_tsc arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf pni pclmulqdq dtes64 monitor ds_cpl vmx est tm2 ssse3 cx16 xtpr pdcm pcid sse4_1 sse4_2 x2apic popcnt tsc_deadline_timer aes xsave avx f16c rdrand lahf_lm cpuid_fault pti ssbd ibrs ibpb stibp tpr_shadow vnmi flexpriority ept vpid fsgsbase smep erms xsaveopt dtherm ida arat pln pts md_clear flush_l1d
