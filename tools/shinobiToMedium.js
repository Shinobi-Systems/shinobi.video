var request = require('request')
var opn = require('opn')
var fs = require('fs')
var medium = require('medium-sdk')
var config = require(__dirname+'/../conf.json')
var articles = Object.values(require(__dirname+'/../database/articles.json'))
var doneAlready = {}
try{
    doneAlready = require(__dirname+'/doneAlready.json')
}catch(err){
    
}

var client = new medium.MediumClient({
  clientId: config.medium.clientId,
  clientSecret: config.medium.clientSecret
})
var redirectURL = 'https://shinobi.video/medium'; 

var theWorkingToken = config.medium.authorizationCode

var createAuthUrl = function(){
    return client.getAuthorizationUrl('secretState', redirectURL, [medium.Scope.BASIC_PROFILE, medium.Scope.PUBLISH_POST])
}

var createPost = function(article){
    if(!article)return console.log('Done');
    var title = trimTitle(article.title)
    console.log(title)
    if(doneAlready[article.id]){
        console.log('Already Posted, Skipping...')
        ++i
        createPost(articles[i])
        return
    }
    client.getUser(function (err, user) {
        if(err){
            console.log('Failed, client.getUser')
            opn(createAuthUrl())
            return
        }
        var content = '<h1>'+title+'</h1>'+article.page__opening+'\n'+article.page__content
        client.createPost({
            userId: user.id,
            title: title,
            contentFormat: medium.PostContentFormat.HTML,
            content: content,
            publishStatus: medium.PostPublishStatus.DRAFT
        },function (err, post) {
            doneAlready[article.id] = article
            fs.writeFileSync(__dirname+'/doneAlready.json',JSON.stringify(doneAlready,null,3));
            ++i
            createPost(articles[i])
        })
    })
}

var trimTitle = function(html){
    return html.replace(/\n/g,'').trim()
}

var i = 0
client.exchangeAuthorizationCode(theWorkingToken, redirectURL, function (err, token) {
    if(err){
        console.log('Failed, New medium.authorizationCode required')
        opn(createAuthUrl())
        return
    }
    console.log(token)
    client.exchangeRefreshToken(token.refresh_token,function(err,data){
        console.log(44,err)
        createPost(articles[i])
    })
})